const html_webpack_plugin = require('html-webpack-plugin');
const mini_css_extract_plugin = require('mini-css-extract-plugin');

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: { minimize: true }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [ mini_css_extract_plugin.loader, 'css-loader' ]
            },
            {
                test: /\.png$/,
                use: 'file-loader?name=./assets/images/[name].[ext]'
            }
        ]
    },
    devServer: {
        historyApiFallback: true
    },
    plugins: [
        new html_webpack_plugin({
            template: './public/index.html',
            filename: './index.html'
        }),
        new mini_css_extract_plugin({
            filename: './src/app.css',
            chunkFilename: 'app.css'
        })
    ]
};
