import React, {Component} from 'react';

class FeaturedCategories extends Component {
    render () {
        return (
            <div className="tmp_a">
                <div className="tmp_b">
                    <h1 className="tmp_c">Shop For</h1>
                    <div>
                        <div className="tmp_d">
                            <div className="tmp_f">
                                <div className="tmp_i"></div>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">Car Accessories</button>
                                    </div>
                                </div>
                            </div>
                            <div className="tmp_g">
                                <div className="tmp_i"></div>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">Books/Videos/CDs</button>
                                    </div>
                                </div>
                            </div>
                            <div className="clear__both"></div>
                        </div>
                        <div className="tmp_e">
                            <div className="tmp_f">
                                <div className="tmp_i"></div>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">Celebration Items</button>
                                    </div>
                                </div>
                            </div>
                            <div className="tmp_g">
                                <div className="tmp_i"></div>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">Health & Beauty</button>
                                    </div>
                                </div>
                            </div>
                            <div className="clear__both"></div>
                        </div>
                        <div className="clear__both"></div>
                    </div>
                    <div className="More__button__wrapper">
                        <button className="More__button">More Categories</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default FeaturedCategories;
