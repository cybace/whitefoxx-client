import React, { Component } from 'react';

class CategoryFeaturedProducts extends Component {
    render () {
        return (
            <div className="tmp_a">
                <div className="tmp_b">
                    <h1 className="tmp_c">Electronics</h1>
                    <div>
                        <div className="tmp_d">
                            <div className="tmp_f">
                                <div className="tmp_i"></div>
                                <p className="tmp_0">AGM X2 Rugged Phone (Leather)</p>
                                <h2 className="tmp_1">£611.42 GBP</h2>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">View</button>
                                    </div>
                                </div>
                                <div className="tmp_x">
                                    <div className="Add__to__cart__button__wrapper">
                                        <button className="Add__to__cart__button">Add to Cart</button>
                                    </div>
                                </div>
                            </div>
                            <div className="tmp_g">
                                <div className="tmp_i"></div>
                                <p className="tmp_0">AGM X2 Rugged Phone (Leather)</p>
                                <h2 className="tmp_1">£611.42 GBP</h2>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">View</button>
                                    </div>
                                </div>
                                <div className="tmp_x">
                                    <div className="Add__to__cart__button__wrapper">
                                        <button className="Add__to__cart__button">Add to Cart</button>
                                    </div>
                                </div>
                            </div>
                            <div className="clear__both"></div>
                        </div>
                        <div className="tmp_e">
                            <div className="tmp_f">
                                <div className="tmp_i"></div>
                                <p className="tmp_0">AGM X2 Rugged Phone (Leather)</p>
                                <h2 className="tmp_1">£611.42 GBP</h2>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">View</button>
                                    </div>
                                </div>
                                <div className="tmp_x">
                                    <div className="Add__to__cart__button__wrapper">
                                        <button className="Add__to__cart__button">Add to Cart</button>
                                    </div>
                                </div>
                            </div>
                            <div className="tmp_g">
                                <div className="tmp_i"></div>
                                <p className="tmp_0">AGM X2 Rugged Phone (Leather)</p>
                                <h2 className="tmp_1">£611.42 GBP</h2>
                                <div className="tmp_x">
                                    <div className="Global__button__wrapper">
                                        <button className="Global__button">View</button>
                                    </div>
                                </div>
                                <div className="tmp_x">
                                    <div className="Add__to__cart__button__wrapper">
                                        <button className="Add__to__cart__button">Add to Cart</button>
                                    </div>
                                </div>
                            </div>
                            <div className="clear__both"></div>
                        </div>
                        <div className="clear__both"></div>
                    </div>
                    <div className="More__button__wrapper">
                        <button className="More__button">More Electronics</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default CategoryFeaturedProducts;
