import React from 'react';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            header_sticky_visible: false
        };
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentWillMount() {
        this.updateDimensions();
    }

    updateDimensions() {
        this.forceUpdate();
    }

    render() {
        var large_width_min = 1024;
        var medium_width_min = 768;

        if (window.innerWidth >= large_width_min) {
            return (
                <div>
                <div className="Header__top__wrapper__LW">
                    <div className="Header__col__50PC__LW">
                        <img className="Header__logo" src="/assets/images/logo.png" />
                    </div>
                    <div className="Header__col__50PC__LW">
                        <div className="Header__options__LW">
                            <select className="Header__options__select__LW">
                                <option disabled selected>Select Language</option>
                                <option>Bar</option>
                                <option>Baz</option>
                            </select>
                            <select className="Header__options__select__LW">
                                <option disabled selected>Select Currency</option>
                                <option>Bar</option>
                                <option>Baz</option>
                            </select>
                        </div>
                        <div className="Header__cart__LW">
                            <div className="Header__cart__wrapper__LW">
                                <div className="Header__cart__top__LW">
                                    <div className="Header__cart__text__LW">View Cart</div>
                                </div>
                                <div className="Header__cart__bottom__LW">
                                    <div className="Checkout__button__wrapper">
                                        <button className="Checkout__button">Checkout</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="Clear__both"></div>
                </div>
                <div className="Header__bottom__wrapper">
                    <div className="Header__search__wrapper__LW">
                        <span className="Icon__search__input"></span>
                        <input className="Header__search__input" type="text" placeholder="Search all products..." />
                    </div>
                    <div className="Header__login__wrapper__LW">
                        <div className="Global__button__wrapper">
                            <button className="Global__button">Log In</button>
                        </div>
                    </div>
                    <div className="Clear__both"></div>
                </div>
                </div>
            );
        } else if (window.innerWidth >= medium_width_min) {
            return (
                <div>
                <div className="Header__top__wrapper__MW">
                    <div className="Header__col__100PC__MW">
                        <img className="Header__logo" src="/assets/images/logo.png" />
                    </div>
                </div>
                <div className="Header__middle__wrapper__MW">
                    <div className="Header__options__MW">
                        <select className="Header__options__select__MW">
                            <option disabled selected>Select Language</option>
                            <option>Bar</option>
                            <option>Baz</option>
                        </select>
                        <select className="Header__options__select__MW__nomarginbottom">
                            <option disabled selected>Select Currency</option>
                            <option>Bar</option>
                            <option>Baz</option>
                        </select>
                    </div>
                    <div className="Header__cart__MW">
                        <div className="Header__cart__wrapper__LW">
                            <div className="Header__cart__top__LW">
                                <div className="Header__cart__text__LW">View Cart</div>
                            </div>
                            <div className="Header__cart__bottom__LW">
                                <div className="Checkout__button__wrapper">
                                    <button className="Checkout__button">Checkout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="Clear__both"></div>
                </div>
                <div className="Header__bottom__wrapper">
                    <div className="Header__search__wrapper__LW">
                        <span className="Icon__search__input"></span>
                        <input className="Header__search__input" type="text" placeholder="Search all products..." />
                    </div>
                    <div className="Header__login__wrapper__LW">
                        <div className="Global__button__wrapper">
                            <button className="Global__button">Log In</button>
                        </div>
                    </div>
                    <div className="Clear__both"></div>
                </div>
                </div>
            );
        } else {
            return (
                <div>
                <div className="Header__wrapper__SW">
                    <div className="Header__col__100PC__SW">
                        <img className="Header__logo" src="/assets/images/logo.png" />
                    </div>
                </div>
                <div className="Header__wrapper__SW">
                    <select className="Header__options__select__SW">
                        <option disabled selected>Select Language</option>
                    </select>
                </div>
                <div className="Header__wrapper__SW">
                    <select className="Header__options__select__SW">
                        <option>Select Currency</option>
                    </select>
                </div>
                <div className="Header__wrapper__SW">
                    <div className="Header__col__100PC__W44__SW">
                        <div className="Global__button__wrapper">
                            <button className="Global__button">Login</button>
                        </div>
                    </div>
                </div>
                <div className="Header__wrapper__SW">
                    <div className="Header__col__100PC__SW">
                        <div className="Header__cart__wrapper__LW">
                            <div className="Header__cart__top__LW">
                                <div className="Header__cart__text__LW">View Cart</div>
                            </div>
                            <div className="Header__cart__bottom__LW">
                                <div className="Checkout__button__wrapper">
                                    <button className="Checkout__button">Checkout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Header__bottom__wrapper">
                    <div className="Header__search__wrapper__SW">
                        <span className="Icon__search__input"></span>
                        <input className="Header__search__input" type="text" placeholder="Search all products..." />
                    </div>
                </div>
                </div>
            );
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }
}

export default Header;
