import Footer from './Footer.js';
import Header from './Header.js';
import React, { Component } from 'react';

const Breadcrumbs = () => (
    <div className="Breadcrumbs">
        <p>Home > Electronics > AGM X2 Rugged Phone (Leather)</p>
    </div>
);

class Product extends Component {
    render() {
        return (
            <div>
                <Header />
                <Breadcrumbs />
                <div className="Content">
                    <div className="Product__wrapper">
                        <div>
                            <div className="Product__col__l"></div>
                            <div className="Product__col__r">
                                <h1 className="Product__name">AGM X2 Rugged Phone (Leather)</h1>
                                <p className="Product__vendor">Sold by Chinavasion</p>
                                <h2 className="Product__price">£611.42 GBP</h2>
                            </div>
                            <div className="clear__both"></div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Product;
