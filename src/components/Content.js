import CategoryFeaturedProducts from './CategoryFeaturedProducts';
import FeaturedCategories from './FeaturedCategories';
import React, { Component } from 'react';

class Content extends Component {
    render() {
        return (
            <div className="App__content">
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
                <FeaturedCategories />
                <CategoryFeaturedProducts />
            </div>
        );
    }
}

export default Content;
