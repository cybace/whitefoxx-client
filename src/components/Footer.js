import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="Footer">
                <ul className="Footer__ul">
                    <li className="Footer__li"><a className="Footer__a" href="#">About Us</a></li>
                    <li className="Footer__li"><a className="Footer__a" href="#">Privacy Policy</a></li>
                    <li className="Footer__li"><a className="Footer__a" href="#">Refunds Policy</a></li>
                    <li className="Footer__li"><a className="Footer__a" href="#">Terms & Conditions</a></li>
                    <li className="Footer__li"><a className="Footer__a" href="#">Copyright</a></li>
                </ul>
            </div>
        );
    }
}

export default Footer;
