import Footer from './Footer.js';
import Header from './Header.js';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const FeaturedCategory = () => (
    <div className="Home__element">
        <h1 className="Home__element__title">Shop For</h1>
        <div>
            <div className="Home__element__l">
                <div className="Home__element__l">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper">
                            <img className="Home__element__item__img" src="/assets/images/category-car-accessories.jpg" />
                        </div>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">Car Accessories</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Home__element__r">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper">
                            <img className="Home__element__item__img" src="/assets/images/category-books.jpg" />
                        </div>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">Books/Videos/CDs</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clear__both"></div>
            </div>
            <div className="Home__element__r">
                <div className="Home__element__l">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper">
                            <img className="Home__element__item__img" src="/assets/images/category-party.jpg" />
                        </div>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">Celebration Items</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Home__element__r">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper">
                            <img className="Home__element__item__img" src="/assets/images/category-health-and-beauty.jpg" />
                        </div>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">Health & Beauty</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clear__both"></div>
            </div>
            <div className="clear__both"></div>
        </div>
        <div className="More__button__wrapper">
            <button className="More__button">More Categories</button>
        </div>
    </div>
);

const FeaturedProductsFromCategory = () => (
    <div className="Home__element">
        <h1 className="Home__element__title">Electronics</h1>
        <div>
            <div className="Home__element__l">
                <div className="Home__element__l">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper">
                            <img className="Home__element__item__img" src="/assets/images/product-phone.jpg" />
                        </div>
                        <p className="Home__element__item__name">AGM X2 RUGGED PHONE (LEATHER)</p>
                        <h2 className="Home__element__item__price">£611.42 GBP</h2>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <Link to='/collections/electronics/products/22276'><button className="Global__button">View</button></Link>
                            </div>
                        </div>
                        <div className="Home__element__products__add__to__cart">
                            <div className="Add__to__cart__button__wrapper">
                                <button className="Add__to__cart__button">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Home__element__r">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper"></div>
                        <p className="Home__element__item__name">AGM X2 RUGGED PHONE (LEATHER)</p>
                        <h2 className="Home__element__item__price">£611.42 GBP</h2>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">View</button>
                            </div>
                        </div>
                        <div className="Home__element__products__add__to__cart">
                            <div className="Add__to__cart__button__wrapper">
                                <button className="Add__to__cart__button">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clear__both"></div>
            </div>
            <div className="Home__element__r">
                <div className="Home__element__l">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper"></div>
                        <p className="Home__element__item__name">AGM X2 RUGGED PHONE (LEATHER)</p>
                        <h2 className="Home__element__item__price">£611.42 GBP</h2>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">View</button>
                            </div>
                        </div>
                        <div className="Home__element__products__add__to__cart">
                            <div className="Add__to__cart__button__wrapper">
                                <button className="Add__to__cart__button">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Home__element__r">
                    <div className="Home__element__item__wrapper">
                        <div className="Home__element__item__img__wrapper"></div>
                        <p className="Home__element__item__name">AGM X2 RUGGED PHONE (LEATHER)</p>
                        <h2 className="Home__element__item__price">£611.42 GBP</h2>
                        <div className="Home__element__item__view">
                            <div className="Global__button__wrapper">
                                <button className="Global__button">View</button>
                            </div>
                        </div>
                        <div className="Home__element__products__add__to__cart">
                            <div className="Add__to__cart__button__wrapper">
                                <button className="Add__to__cart__button">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clear__both"></div>
            </div>
            <div className="clear__both"></div>
        </div>
        <div className="More__button__wrapper">
            <button className="More__button">More Electronics</button>
        </div>
    </div>
);

class Home extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="Content">
                    <FeaturedCategory />
                    <FeaturedProductsFromCategory />
                </div>
                <Footer />
            </div>
        );
    }
}

export default Home;
