import Home from './components/Home.js';
import Product from './components/Product.js';
import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <div>
                <Route exact={true} path='/' component={Home} />
                <Route exact={true} path='/collections/electronics/products/22276' component={Product} />
            </div>
        );
    }
}

export default App;
