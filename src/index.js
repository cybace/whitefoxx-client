import App from './App'
import Client from 'shopify-buy';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './app.css'

const client = Client.buildClient({
    storefrontAccessToken: 'c49f73fa910de00bd6f5622f1154be1a',
    domain: 'whitefoxx.myshopify.com'
});

ReactDOM.render(
    <BrowserRouter>
        <App client={client} />
    </BrowserRouter>,
    document.getElementById('root'));
